package main

import (
	"fmt"
	"bufio"
	"os"
	"strconv"
	YouStack "GolangWorkspace/package/mystack"
)

func unhex(c byte) byte{
	switch  {
	case '0' <= c && c <= '9':
		return c - '0'
	case 'a' <= c && c <= 'f':
		return c - 'a' + 10
	case 'A' <= c && c <= 'F':
		return c - 'A' + 10
	}
	return 0
}
//逆波兰式计算器：
var reader *bufio.Reader = bufio.NewReader(os.Stdin)
var st = new(YouStack.Stack)

func ReversePolishnotation(){
	for {
		fmt.Println("Please input:")
		s, err := reader.ReadString('\n')
		var token string
		if err != nil{
			return
		}
		for _, c := range s{
			switch {
			case c >= '0' && c <= '9':
				token = token + string(c)
			case c == ' ':
				r, _ := strconv.Atoi(token)
				st.Push(r)
				token = ""
			case c == '+':
				fmt.Printf("%d\n",st.Pop()+ st.Pop())
			case c == '*':
				fmt.Printf("%d\n",st.Pop() * st.Pop())
			case c == '-':
				s1 := st.Pop()
				s2 := st.Pop()
				fmt.Printf("%d\n",s1 + s2)
			case c == '/':
				s1 := st.Pop()
				s2 := st.Pop()
				fmt.Println(s2 / s1)
			case c == 'q'://意思是退出
				return
			default:
				//panic("The this operation is error!")
			}
		}
	}
}


func main() {
	/*a.Break_J()
	fmt.Println("Hello World")
	fmt.Println(unhex('a'))
	C := a.MyWorld(10)
	fmt.Println(C(20))
	fmt.Println(a.NiuNiu("I love you"))
	s1 := []int{1, 5, 6,  7 , 3, 2}
	a.Sort(s1)
	fmt.Println(s1)*/
	ReversePolishnotation()
}
