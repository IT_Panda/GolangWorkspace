package constant

const(
	MyName = "xuelei"
)

func MyWorld(x int) func(y int) int{
	return func(y int) int{
		return x * y
	}
}
//先打标签，等循环到了指定的条件以后会从指定标签跳出去
func Break_J(){
J:	for j := 0; j < 5; j++{
	for i := 0; i < 10; i++{
		if i > 5{
			break J
		}
		println(i)
	}
}
}
