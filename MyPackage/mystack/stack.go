package mystack

import "testing"

//功能的实现
type Stack struct {
	i int
	data [10]int
}

func (s * Stack)Push(k int)  {
	s.data[s.i] = k
	s.i++
}

func (s * Stack)Pop() (ret int){
	s.i--
	if s.i < 0{
		s.i = 0
		return
	}
	ret = s.data[s.i]
	return
}

//自己写的包的测试函数
func TestPushPop(t *testing.T){
	c := new(Stack)
	c.Push(5)
	if c. Pop()  != 5{
		t.Log("Pop doesn't give 5")
		t.Fail()
	}
}
