package main

import (
	"fmt"
)

func Coffee(ch chan <-bool){
	flag := false
	fmt.Println("I am is Coffee")
	flag = true
	ch <- flag


}

func Tea(ch chan <-bool){
	flag := false
	fmt.Println("I am is Tea")
	flag = true
	ch <- flag
}
func Channel(){
	ch1:= make(chan bool)
	ch2:= make(chan bool)
	go Coffee(ch1)
	go Tea(ch2)
	select {
	default:
		chi := <-ch1
		chj := <-ch2
		if chi {
			fmt.Println("coffee is true")
		}else{
			fmt.Println("coffee is false")
		}
		if chj{
			fmt.Println("tea is true")
		}else{
			fmt.Println("tea is false")
		}
	}
}
func main() {
	Channel()
}
