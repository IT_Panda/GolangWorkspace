package main

import (
	"context"
	"fmt"
	"os"
)

func KeyStirng(){
	type Keytype string
	var a  Keytype
	var str string
	fmt.Printf("%q\n",str)
	fmt.Printf("%q",a)
	str = string(a)
}

func main() {
	//
	type KeyType string
	//type KeyType = string

	f := func(ctx context.Context, k KeyType) {
		if v := ctx.Value(k); v != nil {
			fmt.Println("found value:", v)
			return
		}
		fmt.Println("key not found: ", k)
	}

	ctx := context.WithValue(context.Background(), "key2", "Value2")
	ctx = context.WithValue(ctx, "key1", "Value1")

	f(ctx, "key1")
	f(ctx, "key2")

	KeyStirng()
	var str string
	fmt.Printf("str : %v\n", str)
	a, err := os.Open("infile")
	// ...
	a, err = os.Create("outfile") // compile error: no new variables
	fmt.Println(a,err)
}