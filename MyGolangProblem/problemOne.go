package main

import (
	"fmt"
)
//问题一 defer
func f1(a, b int) (sum int){
	defer func(){sum = 0}()
	sum = a + b
	return sum
}
//先给返回值赋值，然后调用defer表达式，最后才是返回到调用函数中
func f2(a, b int) (int){
	sum := 0
	defer func(){sum = 0}()
	sum = a + b
	return sum
}
func f3(r int) int{
	defer func(r int){
		r = r + 5
	}(r)
	return r
}
func f4() (r int) {
	i := 0
	defer func(r int) {
		r = r + 5
		fmt.Println("i1 : ",i)
	}(r)
	fmt.Println("i2 : ",i)
	return 1
}

func f5() (result int) {
	i := 0
	defer func() {
		result++
		fmt.Println("i1 : ",i)
	}()
	fmt.Println("i2 : ",i)
	return 0
}

//问题二  对切片的操作，赋值和对切片的扩展
func slice1(){
	a := make([]int, 10)
	b := a
	a = append(a, 10)
	fmt.Println("a1 :", a, "b1 :", b,cap(a),cap(b))
}

func slice2(){
	a := make([] int, 10)
	b := a
	a[5] = 10
	fmt.Println("a2 :", a, "b2 :", b)
}

//nil的使用：
func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

type Ret struct {
	status int    //状态码
	desc   string //失败原因
	source string //数据来源
}
func channel(){
	complete := make(chan Ret)
	baby := Ret{12, "name", "you"}
	baby1 := Ret{12, "name", "you"}
	go func() {
		complete <- baby
		complete <- baby1
		close(complete)
	}()
	count := 0
	for i := range complete{
		count++
		fmt.Println(i)
		fmt.Println(count)
	}
}

func main() {
	/*fmt.Println(f1(10, 20))
	fmt.Println(f2(10,20))
	fmt.Println("f3 :",f3(10))
	fmt.Prinnttln("f4 :",f4())
	fmt.Priln("f5 :",f5())
	slice1()
	slice2()*/

	var done bool//bool默认为false
	fmt.Println(done)
}
