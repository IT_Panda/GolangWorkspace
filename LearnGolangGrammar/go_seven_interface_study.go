package main

import (
	"fmt"
	"unsafe"
	"strconv"
	"reflect"
)

//使用interface的map函数, 相当于把interface作为一个Any类型使用
type e interface {

}
func mult2(f e) e{
	switch f.(type){
	case int:
		return  f.(int) * 2
	case string:
		return f.(string) + f.(string) + f.(string) + f.(string)
	}
	return f
}

func Map(n []e, f func(e) e) []e{
	m := make([]e, len(n))
	for k, v:= range n{
		m[k] = f(v)
	}
	return m
}

type All interface{
	Reader(filename string)int
	Writer(fliename string)int

}

type One interface {
	Reader(str string)string
	Write(*string) []byte
}

type Manager struct{
	name string
	Id int
}

func (manager Manager)Reader(str string) string{
	return manager.name + str + "ID :" + strconv.Itoa(manager.Id)
}

func (manager Manager) Write(str *string) [] byte{
	return []byte( *str + manager.name + "Id :" + strconv.Itoa(manager.Id))
}

func Interface_Manager(Interface_one One) []byte{
	fmt.Printf("%v", Interface_one.Reader("love"))
	Str := "Have"
	return Interface_one.Write(&Str)
}

//排序的int 和 string
//定义统一的接口
type Sorter interface {
	Len() int
	Less(i, j int)bool
	Swap(i, j int)
}
//定义封装的类型
type Xi []int
type Xs []string

//实现Sorter接口的方法，int型的
func (p Xi)Len() int{
	return len(p)
}
func (p Xi)Less(i, j int) bool{
	return p[i] > p[j]
}

func (p Xi)Swap(i, j int){
	p[i], p[j] = p[j] , p[i]
}

//实现字符串的接口的方法：
func (p Xs)Len()int{
	return len(p)
}

func (p Xs)Less(i, j int)bool{
	return p[i] > p[j]
}

func (p Xs)Swap(i, j int){
	p[i], p[j] = p[j], p[i]
}

//编写通用的接口函数：
func Sort(x Sorter){
	for i := 0; i < x.Len()-1; i++{
		for j := i+1; j < x.Len(); j++{
			if x.Less(i, j){
				x.Swap(i,j)
			}
		}
	}
}

//自省和反射
type Person struct{
	name string "namestr"    //"namestr"是标签
	age int
}

func ShowTag(i interface{}){
	switch t := reflect.TypeOf(i); t.Kind(){
	case reflect.Ptr: //一个指针也就是reflect.ptr
	tag := t.Elem().Field(0).Tag
	fmt.Printf("Tag ::   %v\n", tag)
	}
}

func show(i interface{}){
	switch t:= i.(type) {
	case *Person:
		fmt.Printf("%T\n",t)
		t1 := reflect.TypeOf(i)//得到类型的元数据
		v := reflect.ValueOf(i)//得到的实际的值
		tag:= t1.Elem().Field(0).Tag
		name := v.Elem().Field(0).String()
		fmt.Printf("t1.Elem().Field(0).Tag:   %v\n", tag)//得到我的定义的结构的标签值
		fmt.Printf("v.Elem().Field(0).String  : %v\n", name)//最后访问我的结构体内的字符串
	}
}

func main(){
	m := []e{1, 2, 3, 4}
	s := []e{"a", "b", "c", "d"}
	mf := Map(m, mult2)
	sf := Map(s, mult2)
	fmt.Printf("%v\n", mf)
	fmt.Printf("%v\n", sf)

	//接口interface的类型是16个字节
	var interface_e e
	fmt.Printf("%d\n", unsafe.Sizeof(interface_e))

	/*
	interface 作为统一接口的方法，类似于C++中的虚函数，当某个对象与之绑定的方法实现与我们定义的接口方法相一致的时候
	可以作为统一类型接口方法的集合，并且匹配的成员方法之间在返回值和参数类型以及函数名没有任何差异
	*/
	manager := Manager{"xueleilei", 21}
	fmt.Printf("%s",Interface_Manager(&manager))

	//两种interface自定义的方法的排序
	ints := Xi{44, 67, 3, 17, 89, 10, 73, 9, 14, 8}
	strings := Xs{"hello", "world","xiaoyiyi", "niuniu", "leilei"}
	fmt.Printf("%v\n", ints)
	fmt.Printf("%v\n",strings)
	Sort(ints)
	Sort(strings)
	fmt.Printf("%v\n", ints)
	fmt.Printf("%v\n",strings)

	//自省和反射
	personOne := Person{"xuelei", 21}
	show(&personOne)
	ShowTag(&personOne)
}
