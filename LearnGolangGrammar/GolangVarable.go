package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

/*
	go语言中，类型定义在后变义量名定在前，没有char，只有rune(四字节)
*/
/*
var s1,s2 int  = 1,2
var s3,s4 int  = 1,3
简写以下格式
*/
var (
	s1, s2 int = 1, 2
)
var (
	s3, s4 = 1, 3
)

//简写
var ss = "hello baby"
var bb = false
var qq = 3.33

//普通的定义
func ErI() {
	var a int = 1
	fmt.Printf("%d\n", a)
}

//只有在函数中才能 :=的用法
func PRin() {
	s1 := 1
	s2 := "mybaby"
	s3, s4, s5 := false, "I love You", 0.999
	fmt.Println(s1, s2, s3, s4, s5)
}

//complex的类型的使用
func euler() {
	//复数c
	c := 3 + 4i
	fmt.Println(cmplx.Abs(c)) //求绝对值c
	//欧拉公式
	fmt.Println(cmplx.Pow(math.E, 1i*math.Pi) + 1) //i前面加1，就为1i了
	fmt.Println(cmplx.Exp(1i*math.Pi) + 1)
	fmt.Printf("%.3f\n", cmplx.Exp(1i*math.Pi)+1)
}

//强制类型转换
func triangle() {
	var a, b int = 3, 4
	var c int
	c = int(math.Sqrt(float64(a*a + b*b)))
	fmt.Println(c)
}

//常量.go语言中大写可以在整个包中使用，类似与C++中的public
func consts() {
	const filename = "abc.txt"
	const a, b = 3, 4
	/*
		简写为：
		const(
			filename = "abc.txt"
			a, b = 3, 4
		)
		//go语言中常量只是个文本，const数值可以作为各种类型使用
	*/
	var c int
	c = int(math.Sqrt(a*a + b*b))
	fmt.Println(filename, c)
}

//特殊的常量---枚举类型
func enums() {
	/*
		const (
			c      = 0
			cpp    = 1
			java   = 2
			python = 3
			golang = 4
		)
		fmt.Println(c, cpp, java, python, golang)
	*/
	//使用自增值iota,iota从0开始
	const (
		c = iota
		cpp
		_ //这个没有打印，但是也算一份子
		java
		python
		golang
		myname
	)
	fmt.Println(c, cpp, java, python, golang, myname)
	//b,kb,mb,gb,tb,pb
	//另一种const的使用
	const (
		b = 1 << (10 * iota)
		kb
		mb
		gb
		tb
		pb
	)
	fmt.Println(b, kb, mb, gb, tb, pb)
}

//if 的使用     go 语言的if不适用括号的()
func bounder(v int) int {
	if v > 100 {
		return 100
	} else if v < 0 {
		return 0
	} else {
		return v
	}
}
func main() {
	/*
		fmt.Println("Hello, World baby!")
		fmt.Println(s1, s2)
		ErI()
		PRin()
		euler()

	*/
	triangle()
	consts()
	enums()
	fmt.Println(bounder(103))
}
