package main

import (
	"fmt"
	"time"
)
func Range(){
	Map := map[string] string{"a":"my", "b":"name"}
	for i := range Map{
		fmt.Println(i, Map[i])
	}
}

//go语言可变参数
func Sum(nums ...int){
	fmt.Print(nums, " ")
	total := 0
	for _ , num := range nums{
		total += num
	}
	fmt.Println(total)

}
//go语言闭包，匿名函数实例
func intSeq() func() int{
	i := 0
	return func() int{
		i += 1
		return i
	}
}

//阶乘
func Fact(num int) int{
	if num <= 1 {
		return 1
	}
	return num* Fact(num-1)
}

func F(value string){
	for i := 0; i < 3; i++{
		fmt.Println(value, ": ", i)
	}
}

//go语言通道
func Pipe(){
	message := make(chan string)
	go func(){message <- "ping"}()
	msg := <-message
	fmt.Println(msg)
}

//通道路线实例
func ping(pings chan <- string,  msg string){
	pings <- msg
}
func pong(pings <- chan string, pongs chan  <- string){
	msg := <-pings
	pongs <- msg
}

//go   select实例
func Select(){
	c1 := make(chan string)
	c2 := make(chan string)
	go func(){
		time.Sleep(time.Second * 1)
		c1 <- "one"
	}()
	go func(){
		time.Sleep(time.Second * 2)
		c2 <- "two"
	}()

	for i := 0; i < 2; i++{
		select{
			case  msg1 := <- c1:
			fmt.Println("received", msg1)
			case  msg2 := <- c2:
				fmt.Println("received", msg2)
			}
	}
}

//go超时实例

func TimeOut(){
	c1 := make(chan string, 1)
	go func(){
		time.Sleep(time.Second *  2)
		c1 <- "result 1"
	}()

	select {
	case res := <-c1:
		fmt.Println(res)
	case <-time.After(time.Second * 1):
		fmt.Println("timeout 1")
	}
	c2 := make(chan string , 1)
	go func(){
		time.Sleep(time.Second * 2)
		c2 <- "result 2"
	}()
	select {
	case res := <-c2:
		fmt.Println(res)
	case <-time.After(time.Second * 3):
		fmt.Println("timeout 2")
	}
}

//非阻塞通道操作实例
func Non_Blocking(){
	messages := make(chan string)
	signals := make(chan bool)

	select {
	case msg := <-messages:
		fmt.Println("received message", msg)
	default:
		fmt.Println("no message received")
	}

	msg := "hi"
	select {
	case messages <-msg:
		fmt.Println("sent message", msg)
	default:
		fmt.Println("no message snet")
	}

	select {
	case kk := <- signals:
		fmt.Println("signal:", kk)
	default:
		fmt.Println("Not signal")
	}
}
func One_Array(){

	var arr [10]int
	for i := 0; i < 10; i++{
		arr[i] = i+1
	}
	for i:= range arr{
		fmt.Println(arr[i])
	}
	array := [...]int{1,2,3,4}
	fmt.Println(len(array))
}

func Show(array  []int){
	for i:= range array{
		fmt.Println(array[i])
	}
}

func Copy() {
	src := [5]string{"a", "b"}
	var des [5]string
	des = src
	fmt.Println(des)
	var arr1 [5]string
	arr2 := [5]string{"1","2","3","4","5"}
	arr1 = arr2
	fmt.Println(arr1)
}
func main(){
	/*
	fmt.Println("helloworld")
	var str []string = make([]string,  4)
	fmt.Println(str)
	Range()
	nums := []int{1, 2, 3, 4, 5, 6}
	Sum(1,2,3,4,5)
	Sum(nums...)
	nextInt := intSeq()
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	next_Second := intSeq()
	fmt.Println(next_Second())
	fmt.Println(Fact(3))
	//并发程序
	F("direct")
	go F("goroutine")
	go func(msg string){
		fmt.Println(msg)
	}("going")
	var input string
	fmt.Println(&input)
	fmt.Println("done")
	go Pipe()
	//通道路线
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings ,"passwed. message")
	pong(pings, pongs)
	fmt.Println(<-pongs)
	Select()
	One_Array()
	array := []int{1:1, 2 :5}
	Show(array)
	*/
	Copy()
	TimeOut()
}