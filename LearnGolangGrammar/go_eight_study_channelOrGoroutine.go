package main

import (
	"fmt"
)

//并行是关于性能的
//并发是关于程序设计的
//goroutine
var c chan int
func ready(w string, sec int){
	//time.Sleep(time.Duration(sec) * time.Second)
	i := 0
	L: for{
		select{
		case <-c:
			i++
			if i > 1{
	 			break L
			}
		}
	}
	fmt.Println(w, "is ready")
	c <- 1
}
//对同一代码进行操作必须保证原子性，同步控制
func Print(ch chan int){
	fmt.Printf("GolangWorkspace\n")
	ch <- 1
}
/*定义只读的channel
	read_only := make( <- chan int)
定义只写的channel
write_only := make( chan <- int)
定义可同时读写的channel
read_write := make(chan int)
*/

func Read_only(send <- chan int){
	for i := range send{
		fmt.Println(i)
	}
}
func Write_only(write chan<- int){
	for i := 0; i<10; i++{
		write <- i
	}
}
//测试并发的计算效率
func sum(value []int, chane chan int){
	sum := 0
	for i := range value{
		sum += value[i]
	}
	chane <- sum
}
func main(){
	/*
	c = make(chan int)//一定要初始化
	go ready("Tea", 2)
	go ready("Coffee", 1)
	fmt.Println("I'm waiting")
	time.Sleep(5 * time.Second)
	*/
	//main函数写，函数调用读

	/*chs := make([]chan int, 10)
		for i := 0; i < 10; i++{
			chs[i] = make(chan int)
			go Print(chs[i])
		}
		for _, ch := range chs{
			fmt.Println(<- ch)
		}*/

	//只读只写函数管道的模拟
	/*var channel chan int = make(chan int)
	go Write_only(channel)
	go Read_only(channel)
	time.Sleep(3 *time.Second)*/

	//测试并发的计算效率的测试用例
	values := []int{1, 2, 3, 4, 5, 6, 7, 8}
	chael := make(chan int , 2)
	go sum(values[: len(values)/2], chael)
	go sum(values[len(values)/2 :], chael)
	sum1, sum2 := <-chael, <-chael
	fmt.Println(sum1, sum2)
}


