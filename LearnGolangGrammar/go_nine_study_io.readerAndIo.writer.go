package main

import (
	"os"
	"net/http"
	"fmt"
	"io"
	"bufio"
	"net"
	"io/ioutil"
)

//---------------------------------------------------用来做通讯知识练手用的

//HTTP客户端
//demo请求一个网站的首页，并将首页内容打印到标准输出流中
func DemoGo(){
	resp,err:=http.Get("http://example.com/")
	if err!=nil{
		fmt.Println("request fail")
		return
	}
	defer resp.Body.Close()
	io.Copy(os.Stdout,resp.Body)
}

func OpenFile(){
	//进行文件操作，并将文件中的内容打印到屏幕上边去
	buf := make([]byte, 1024)
	f, _ := os.Open("./Os.txt")
	defer f.Close() //延迟关闭的一个命令函数吧，挺好用的
	for {
		n, _ := f.Read(buf)
		if n <= 0{
			break
		}
		os.Stdout.Write(buf[:n])//若不加  :n  则会发生UTF_8字符和ascll字符冲突不在一行
	}
}
//利用bufio包，把打开的文件描述符转换为有缓冲的reader
func Bufio_file(file string){
	buff := make([]byte, 1024)
	f,err := os.Open(file)
	defer f.Close()
	if err != nil{
		fmt.Println("The open file is false")
	}
	wr := bufio.NewWriter(os.Stdout)
	re := bufio.NewReader(f)
	defer wr.Flush()
	for {
		n, _ := re.Read(buff)
		if n == 0{
			break
		}
		wr.Write(buff[:n])
	}
}

//网络net的远程控制：
func Net(){
	conn , e := net.Dial("tcp", "192.0.32.10:80")
	conn1, e1 := net.Dial("udp", "192.0.32.10.80")
	fmt.Println(conn, e)
	fmt.Println(conn1, e1)
}

//http包，http Get
func HttpGet(){
	r, err := http.Get("http://www.geogle.com/robots.txt")//使用http获取html
	if err != nil{
		fmt.Printf("%s\n",err)
		return
	}
	b, err := ioutil.ReadAll(r.Body)//把整个内容读入b
	r.Body.Close()
	if err == nil{
		fmt.Printf("%s\n",string(b)) //没有错误全部打印
	}
}

func main() {
	/*OpenFile()
	DemoGo()*/
	//Bufio_file("./Os.txt")
	Net()
	HttpGet()
}
