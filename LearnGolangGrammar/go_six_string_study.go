package main

import (
	"fmt"
	"unicode/utf8"
)

/*
	rune相当于go中的char
使用range遍历pos， rune对
使用utf8， RuneCountInstring获得字符数量
使用len获得字节长度
使用[]byte获得字节
 */
func String_bytes(s string){
	fmt.Println(s)
	for i, v := range []byte(s){
		fmt.Printf("(%d , %X)", i, v)
	}
	fmt.Println()
	for _, v := range s{
		fmt.Printf("%x  ", v)
	}
	fmt.Println("\n","Rune count:",utf8.RuneCountInString(s))
}

func UTF_8_byte(s string){
	bytes := []byte(s)
	for len(bytes) > 0{
		ch, size := utf8.DecodeRune(bytes)
		bytes = bytes[size:]
		fmt.Printf("%c", ch)
	}
	fmt.Println()
	for i, ch := range []rune(s){
		fmt.Printf("(%d   %c)",i ,ch)
	}
	fmt.Println()
}


//寻找最长不含有重复字符的子串--------在上一个代码的基础上为了解决中文重复
func lengthOfNonRpaeatingSubStr_rune(s string) int{
	lastOccurred := make(map[rune] int)
	start := 0
	maxLength := 0

	for i, ch := range []rune(s){
		if lastI, ok := lastOccurred[ch]; ok && lastI >= start{
			start = lastI + 1
		}
		if i - start +21 > maxLength{
			maxLength = i - start + 1
		}
		lastOccurred[ch] = i
	}
	return maxLength
}
func main(){
	fmt.Println("string study: ")
	String_bytes("love西山居!")
	UTF_8_byte("love西山居!")
	fmt.Println(lengthOfNonRpaeatingSubStr_rune("一二三一二三一二"))
}
