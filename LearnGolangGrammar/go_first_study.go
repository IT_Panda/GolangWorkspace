package main


import (
	"fmt"
	"unsafe"
	"strings"
	_ "os"
	"errors"
	"math"
)

/*
练手的小文件
 */


var a int
var b int
var(
	aa int
	bb string
)

func Fun(){
	 a, b,string:= 1, 3, "hell"
	// var str string = "hello world"
	 fmt.Println(a, b, string)
}
func Return(a, b int)(int , int){
	return a,b
}
//类型rune 和 byte
func Rune_byte() {
	var a rune = 'b'
	var b byte = '\n'
	var c uint
	fmt.Println(a, b,c)
}
//类型float64和float32,以及强转的使用
func Float(a float64, b float32) float64{
	return a + float64(b);
}
//枚举类型enum，自增长
func Enum(){
	const(
		C = iota
		_
		Java
		Go
	)
	fmt.Println(C ,Java, Go)
}


func Consts(){
	const a = "you is my baby"
	const b = 'A'
	fmt.Println(a,"\n", b)//加了"\r"导致缓冲区中的值被刷新，所以打印不出来，只能换成了"\n"
	var left = 20
	left++//支持加加操作
	left--//支持减减操作
	const right = 30
	area := left * right
	fmt.Printf("The area is %d %d\n", area, unsafe.Sizeof(left))
	fmt.Printf("The left Type is %T\n",left)
	fmt.Printf("right is of type %T\n", right)
}
func IF_Make(a int , b int){
	if a > b{
		fmt.Printf("%d\r", a)
	}else{
		fmt.Printf("%d\r",b)
	}
}
//switch 的使用
func Switch(a int , b int, str string) int{
	switch str {
	case "*":
		return a *b
	case "-":
		return a - b
	case "+":
		return a + b
	case "/":
		return a / b
	default:
		return -1
	}
}

func UTF_8(){
	var greeting = "GolangWorkspace"
	fmt.Printf("normal string: ")
	fmt.Printf("%s",greeting)
	fmt.Printf("\n")
	fmt.Printf("hex bytes:  ")
	for i := 0; i < len(greeting); i++{//len()函数调用是求字符串或者数组的大小
		fmt.Printf("%x  ", greeting[i])
	}
	fmt.Printf("\n")
	const sampleTest = "\xbd\xb2\x3d\xbc\x20\xe2\x8c\x98"
	fmt.Printf("quoted string:")
	fmt.Printf("%+q", sampleTest)//q标志转义不可打印的字符，+标志它转义非ascii字符以使输出明确无误
	fmt.Printf("\n")
}
func For_Use(n int)int{
	sum := 0
	i := 0
	for ; i< n; i++{
		sum += i
	}
	return sum
}

//连接字符串join()方法,    Join连接数组的元素以创建单个字符串。第二个参数是分隔符，放置在数组的元素之间
func Join(){
	greetings := []string{"Hello", "World!"}
	var str string  = strings.Join(greetings, " ")
	fmt.Printf(str)
	//fmt.Printf("\n")
}

func Array(arr[] int){
	var a , b int
	for ; a  < len(arr); a++{
		arr[a] = a <<  5
	}

	for ; b < len(arr); b++{
		fmt.Printf("%x\n", arr[b])
	}
}

func Pointer(){
	var p *int
	fmt.Printf("%d\n",unsafe.Sizeof(p))
	var a int = 10
	var q *int  = &a
	fmt.Printf("&a = %0x  pointer(a) = %x\n", &a, q)
	fmt.Printf("a = %d\n", *q)
	var ptr *int =  &a
	if ptr == nil {
		fmt.Printf("ptr : %x\n", ptr)
	} else{
		fmt.Printf("The ptr is not nil\n")
	}
}

//go语言中结构体的使用
type student struct {
	Id int
	name string
	score float32
}

func Printf_Struct(std1 *student){
	fmt.Printf("The student Id is %d\n", std1.Id)
	fmt.Printf("The student name is %s\n", std1.name)
	fmt.Printf("The student score is %f\n",std1.score)
}

func PrintSlice(x []int){
	fmt.Printf("len = %d cap = %d slice = %v\n", len(x), cap(x), x)
}

//range的使用,  range相当于数组的遍历的一种索引
func Range(){
	//creat a slice
	numbers := []int{1 , 2, 3, 4}
	//print th numbers
	for i := range numbers{
		fmt.Println("slice item", i , "is", numbers[i])
	}

	//create a map
	I_Love_You := map[string] string{"you name":"myname", "hename": "shename", "niuniu":"xuelei"}
	I_Love_You["yingying"] = "leilei"
	for all := range I_Love_You{
		fmt.Println(all, "love", I_Love_You[all])
	}
	//Print map using key_value
	//删除键值对，delete函数
	delete(I_Love_You, "niuniu")
	capital, Ok := I_Love_You["niuniu"]
	if Ok{
		fmt.Println(capital)
	}else{
		fmt.Println("The name is not find")
	}
	student := map[string] int{"xuelei":21, "hename": 22}
	for name := range student{
		fmt.Println(name ,"is ID", student[name])
	}
}

//go语言式的递归,斐波那契数列
func Recursive_Fibonaci(num int) (ret int){
	if num < 1{
		return 0
	}
	if num == 1{
		return 1
	}
	return Recursive_Fibonaci(num - 1) + Recursive_Fibonaci(num - 2)
}

//go语言中错误的处理
func Sqrt(value float64) (float64, error){
	if value < 0{
		return 0, errors.New("The Value is  illegal")//不合法
	}else{
		return math.Sqrt(value), nil
	}
}
//捕获到出错的地方并提示给用户，使之得到提到反馈，写成两个函数调用，代码复用高
func Error(value float64){
	result, error := Sqrt(value)
	if(error == nil){
		fmt.Println(result)
	}else{
		fmt.Println(error)
	}
}
func main() {
	/*
	fmt.Println(a, b, aa, bb)
	Fun()
	a1,a2 := Return(10, 20)
	fmt.Println(a1,a2)
	fmt.Println(Float(23.121, 32.3))
	Rune_byte()
	Enum()
	Consts()
	IF_Make(10, 20)
	fmt.Println(Switch(10, 20 , "*"))
	fmt.Println(For_Use(10))
	UTF_8()
	Join()
	var arr = [] int{1, 2, 3, 4, 5}
	Array(arr)
	Pointer()
	*/
	//结构体指针的使用
	std1 := student{10, "xueleilei", 99.1}
	var std_ptr *student = &std1
	Printf_Struct(std_ptr)
//len返回 的是整个数组的大小，不管你有没有使用
	var array= [5]int{1, 2, 3}
	for i := len(array)-1; i >= 0; i--{
		fmt.Println(array[i])
	}
	var numbers = make([]int, 2, 5)
	PrintSlice(numbers)
	//切片遵守左闭右开    子切片
	/*
	PrintSlice(numbers[:2])
	PrintSlice(numbers[0:1])
	*/
	number1 := make([]int, len(numbers), (cap(numbers)) * 2)
	PrintSlice(number1)
	numbers = append(numbers, 3,4,5,6,7)
	copy(number1, numbers)
	PrintSlice(number1)
	PrintSlice(numbers)
	Range()
	for i := 0; i < 11; i++{
		fmt.Println(Recursive_Fibonaci(i))
	}

	Error(16)
	Error(-1)
}