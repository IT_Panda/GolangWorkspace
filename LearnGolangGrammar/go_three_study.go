package main

import (
	"fmt"
	"reflect"
	"runtime"
	"math"
)

func Div(a, b int)(q, r int){
	return a / b, a % b
}

func Eval(a, b int, op string) (int , error){
	switch op {
	case "+":
		return a + b , nil
	case "-":
		return a - b, nil
	case "*":
		return a * b, nil
	case "/":
		return a / b, nil
	default:
		return -1, fmt.Errorf("unsupported operation: %s", op)
	}
}

func Apply(op func(int, int) int , a, b int) int{
	p := reflect.ValueOf(op).Pointer()//通过reflect包反射出op函数的函数指针，然后通过该指针指向我所在的函数名
	opName := runtime.FuncForPC(p).Name()//反射出来的函数名
	fmt.Printf("Calling function %s with args" + "(%d, %d)", opName, a, b)
	return op(a , b)
}

func Pow(a , b int) int{
	return int(math.Pow(float64(a), float64(b)))
}
//可变参数
func SumArgs(values ...int) int{
	sum := 0
	for i := range values{
		sum += values[i]
	}
	return sum
}
//数组的定义
func Array_Two(){
	var arr1 [5]int
	arr2 := [3]int{1, 2, 3}
	arr3 := [...]int{2, 4, 6, 8, 10}//...表示让编译器来帮我数数组的大小
	var grid [4][5]int
	fmt.Println(arr1, arr2, arr3)
	fmt.Println(grid)
	//第一种数组的遍历方法
	for i := 0; i < len(arr3); i++{
		fmt.Println(arr3[i])
	}
	//第二种数组的遍历方法
	for i:= range arr3{
		fmt.Println(arr3[i])
	}
	//第三种数组的遍历方法
	for i, v := range arr3{
		fmt.Println(i , v)
	}
	//在第三种方法上的改进，只要值，不要下标
	for _, v := range arr3{ //可以通过   _  下划线省略变量
  		fmt.Println(v)
	}
}

//值传递,go语言没有引用，只有值传递，也就是value 和 pointe
//比较好的一种, swap
func Value_Swap(a , b int)(int, int){
	return b , a
}
//指针传递 ,然后进行两个值得修改
func Value_Pointe(a, b *int){
	var tmp int
	tmp = *a
	*a = *b
	*b = tmp
}
//数组的值传递， 直接把整个数组从实参传给了形参
//只是改变了形参中的值
func Transfer(arr [5]int){
	arr[0] = 100
	for i:= range arr{
		fmt.Println("  ",arr[i])
	}
}
//下面这个使用了指针传递的方式，通过指针改变了存储值位置的那块值，传指针，解引用，但是go语言不解引用也可以
func Transfer_pointe(arr *[5]int){
	arr[0] = 100
	for i := range arr{
		fmt.Println("  ", arr[i])
	}
}


func main(){
	fmt.Println(Div(10 ,20))
	fmt.Println(Eval(10, 20, "*"))
	a, _ := Eval(20 ,10, "/")
	fmt.Println(a)
	if result, err := Eval(3, 4, "x"); err != nil{
		fmt.Println(err)
	}else{
		fmt.Println(result)
	}
	fmt.Println(Apply(Pow,3, 4))
	fmt.Println(Apply(func (a int, b int)int{
		return int(math.Pow(float64(a) , float64(b)))
	}, 3, 4))

	fmt.Println(SumArgs(1,2,3,4,5,6,7))
	Array_Two()
	fmt.Println(Value_Swap(10, 20))
	pointe_a := 10
	pointe_b := 20
	Value_Pointe(&pointe_a, &pointe_b)
	fmt.Println(pointe_a, pointe_b)

	array := [...]int{1, 2, 3, 4, 5}
	fmt.Println("Transfer(array [] int)  :")
	Transfer(array)
	fmt.Println(array)
	fmt.Println("Transfer_pointe(array *[] int)")
	Transfer_pointe(&array)
	fmt.Println(array)
}
