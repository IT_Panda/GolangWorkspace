// 循环和if的使用 project main.go
package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

//文件的读
func File() {
	//filename 自己在本地已创建文件，尝试读
	const filename = "abc.txt"
	//返回的contents是文件的内容，err是返回出错的提示
	/*contents, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%s\n", contents)
	}*/
	//运行前半句，然后判断err是不是空，为空则打印，nil表示无值，类似与C的false
	if contens, err := ioutil.ReadFile(filename); err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%s\n", contens)
	}
}

//switch ,go语言中自动的break，除非使用fallthrough
func eval(a, b int, op string) int {
	var result int
	switch op {
	case "+":
		result = a + b
	case "-":
		result = a - b
	case "*":
		result = a * b
	case "/":
		result = a / b
	default:
		panic("unsupported operator:" + op) //相当于报错
	}
	return result
}

//switch 的另一种使用
func grade(score int) string {
	g := ""
	switch { //switch后面可以没有表达式
	case score < 0 || score > 100:
		panic(fmt.Sprintf("Wrong score: %d", score)) //感觉有点像中断的感觉
	case score < 60:
		g = "D"
	case score < 80:
		g = "C"
	case score < 90:
		g = "B"
	case score <= 100:
		g = "A"
	default:
		panic(fmt.Sprintf("Wrong score:%d", score))
	}
	return g
}

/*
for的条件里不需要括号，
for的条件里可以省略初始条件，结束条件，递增表达式
*/
func Make_for(a int) int {
	sum := 0
	for i := 1; i <= a; i++ {
		sum += i
	}
	return sum
}

//十进制转二进制
func convertToBin(n int) (result string, a int) {
	//result := ""
	for ; n > 0; n /= 2 {
		lsb := n % 2
		result = strconv.Itoa(lsb) + result
		a += 1
	}
	return
}
func main() {

		File()
		fmt.Println(eval(3, 4, "*"))
		fmt.Println(grade(99))

	fmt.Println(Make_for(10))
	var a, b = convertToBin(5)
	fmt.Println(a, b)
	//fmt.Println(a int = convertToBin(5), convertToBin(13))

}
