package main

import (
	"fmt"
)

//切片相当于是数组的一个视图view，也就是我认为的一个子集，但是这个子集与数组也有一定的关系，当子集发生改变的时候，数组也会发生改变
//以后传数组的时候传数组的整个视图，如array[:]
func Slice_Update(array []int){
	array[0] = 100
}

func Print_Slice(s []int){
	fmt.Printf("s = %v  len(s)= %d  cap(s) = %d\n", s, len(s), cap(s))
}
func main(){
	fmt.Println("Slice study:")
	//验证上面说得那句话
	array := [...]int{0, 1, 2, 3, 4, 5, 6, 7}
	s1 := array[2:]
	s2 := array[:]
	fmt.Println("Update went array:")
	fmt.Println(array)
	fmt.Println("s1:", s1)
	fmt.Println("s2:", s2)
	fmt.Println("Update the slice ")
	Slice_Update(s1)
	Slice_Update(s2)
	fmt.Println("s1:", s1)
	fmt.Println("s2:", s2)

	//练习题  Slice 的扩展   说明切片的时候，是可以向后扩展的，访问的时候不能超过len(arr),,否则会发生数组越界，扩展的时候是不能超过cap(arr)
	arr := [...]int{0,1,2,3,4,5,6,7}
	arr_s1 := arr[2:6] //  s1 = {2, 3, 4, 5]
	arr_s2 := s1[3:5] // s2 = {5 , error]
	fmt.Println("arr_s1 :" , arr_s1) //  2  3  4  5
	fmt.Println("arr_s2 :", arr_s2)  //  5  6
	fmt.Println(len(arr_s2), cap(arr_s2)) // 2  3
	fmt.Println(len(arr_s1), cap(arr_s1)) // 4  6

	//slice的增加元素
	/*
	1.go语言有垃圾回收机制，如果动态数组不使用，系统会自动回收
	2.当数组的append的时候，扩展导致了数组的大小超过了cap(arr)，那么的话系统会重新分配一块更大的底层数组给我们使用
	 */
	arr_s3 := append(arr_s2, 10)
	arr_s4 := append(arr_s3, 11)
	arr_s5 := append(arr_s4, 12)
	fmt.Println("arr_s3, arr_s4, arr_s5: ",arr_s3, arr_s4, arr_s5)
	fmt.Println("arr= ", arr)  // why

	var slice_append []int
	Print_Slice(slice_append)
	fmt.Println("zero value Slice_append:", slice_append, len(slice_append), cap(slice_append))
	for i := 0; i<100; i++{
		fmt.Printf("len(slice_append) : %d   cap(slice_append) : %d\n ", len(slice_append), cap(slice_append))
		slice_append = append(slice_append, i * 2 + 1)
	}
	fmt.Println(slice_append)
	//开辟新的数组
	slice_s1 := make([]int, 10)
	slice_s2 := make([]int, 10, 16)
	slice_s3 := []int{1 ,2 ,3 ,4}
	Print_Slice(slice_s1)
	Print_Slice(slice_s2)
	Print_Slice(slice_s3)
	//进行slice之间的copy
	fmt.Println("copy the slice:  ")
	copy(slice_s1, slice_s3)
	Print_Slice(slice_s1)

	//进行slice的删除，数据元素
	fmt.Println("Deleting elements from slice:")
	slice_s1 = append(slice_s1[:1], slice_s1[2:]...)
	Print_Slice(slice_s1)

	fmt.Println("Popping from front:")//头删除会把len的长度降低，并且会把cap的长度也降低，我觉得原因是slice对于数组的扩展只能向后扩展
	front := slice_s1[:1]
	slice_s1 = slice_s1[1:]
	//slice_s1 = slice_s1[1:]
	fmt.Println("front :", front)
	Print_Slice(slice_s1)

	fmt.Println("Popping from back:")//尾删除会降低len的长度，cap的长度不会降低
	back := slice_s1[len(slice_s1) - 1]
	//slice_s1 = append(slice_s1[:len(slice_s1)-1])  //与下面那个等价
	slice_s1 = slice_s1[:len(slice_s1) -1]
	fmt.Println("back : ",back)
	Print_Slice(slice_s1)
}
