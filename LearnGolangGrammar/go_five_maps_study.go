package main

import (
	"fmt"
)

//寻找最长不含有重复字符的子串     例题
func lengrhOfNonRepeatingSubStr(s string) int{
	lastOcuurred := make(map[byte]int)
	start := 0
	maxlength := 0
	for i, ch := range []byte(s){
		if lastI, ok := lastOcuurred[ch]; ok && lastI >= start{
			start = lastI + 1
		}
		if i - start +1 > maxlength{
			maxlength = i -start +1
		}
		lastOcuurred[ch] = i
		}
		return maxlength
}


func main(){
	fmt.Println("study  maps:")
	m1 := map[string]string{
		"name":"xuelei",
		"Id":"21",
		"score": "99",
	}
	m2 := make(map[string]int)//m2 = empty map
	var m3 map[string]int // m3  = nil   在go语言中nil可以参与运算时合法的
	fmt.Println(m1, m2, m3)
	//map的遍历
	fmt.Println("Traversing map:")
	for k ,v := range m1{
		fmt.Println(k , v)
	}
	fmt.Println("false_name is put:")
	get_false_name := m1["false_name"]
	fmt.Println(get_false_name)

	if get_true_name, ok := m1["name"]; ok{
		fmt.Println("get_true_name",get_true_name)
		fmt.Printf("%T\n",get_true_name)
	}else{
		fmt.Println("ok :", ok)
	}
	delete(m1, "name")
	if get_true_name, ok := m1["name"]; ok{
		fmt.Println("get_true_name",get_true_name)
		fmt.Printf("%T\n",get_true_name)
	}else{
		fmt.Println("ok :", ok)
	}
	//寻找最长不含有重复字符的子串：
	fmt.Println(lengrhOfNonRepeatingSubStr("abcabca"))
	fmt.Println(lengrhOfNonRepeatingSubStr("pwaawo"))
	fmt.Println(lengrhOfNonRepeatingSubStr("pppppp"))

}
