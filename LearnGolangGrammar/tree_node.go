package main

import "fmt"

func Range(str []int){
	fmt.Println(str)
}
type Node struct{
	data int //数据
	leftchild *Node //左孩子结点
	rightchild *Node // 右孩子结点
}

func Return_Node_Pointe(val int) *Node{
	return &Node{val,nil, nil}
}

func(node *Node) Setvalue(value int){
	if node == nil{
		fmt.Println("the node is nil")
		return
	}
	node.data = value
}
func (node Node) print(){
	fmt.Println(node.data)
}

func (node *Node) setValue(value int){
	node.data = value
}

func (node *Node) traverse(){
	if node == nil{
		return
	}
	node.leftchild.traverse()
	node.print()
	node.rightchild.traverse()
}

func main() {
	str := [5]int{1 ,2 , 3, 4}
	Range(str[:])
	data := 10
	leftChild := Node{data, nil,nil}
	rightChild := Node{data,nil,nil}
	root := Node{data, &leftChild,&rightChild}
	leftChild.leftchild = &Node{}
	leftChild.leftchild = &Node{data,nil, nil}
	fmt.Println(root)
	fmt.Println(root.leftchild, root.rightchild)
	nodes := []Node{
		{data, nil, nil},
		{data:data},
		{data,nil, &root},
	}
	fmt.Println(nodes)
	root.print()
	root.setValue(100)
	fmt.Println(root.data)

	proot := &root
	proot.print()
	proot.setValue(20)
	proot.print()
	root.traverse()
}