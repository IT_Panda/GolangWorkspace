package main

import (
	"os/exec"
	"strings"
	"sort"
	"fmt"
	"strconv"
	"bufio"
	"os"
	"net"
)

//		----------------------------------------------通讯的模块以及利用os包进行编程
//利用go语言显示进程，类似于ps功能吧
func ProcessGo(){
		ps := exec.Command("ps", "-e", "-opid,ppid,comm")
		output, _ := ps.Output()
		child := make(map[int][]int)
		for i, s := range strings.Split(string(output), "\n"){
			if i == 0 { continue } // Kill first line
			if len(s) == 0 { continue } // Kill last line
			f := strings.Fields(s)
			fpp, _ := strconv.Atoi(f[1]) // 父 pid
			fp, _ := strconv.Atoi(f[0]) // 子 pid
			child[fpp] = append(child[fpp], fp)
		}
		schild := make([]int , len(child))
		i := 0
		for k, _ := range child { schild[i] = k ; i++ }
		sort.Ints(schild)
		for _, ppid := range schild {
			fmt.Printf("Pid %d has %d child", ppid, len(
				child[ppid]))
			if len(child[ppid]) == 1 {
				fmt.Printf(": %v\n", child[ppid])
				continue
			}
			fmt.Printf("ren: %v\n", child[ppid])
		}
}

//单词统计：
func GetTheWords(){
	var chars, words, lines int
	r := bufio.NewReader(os.Stdin)
	for{
		switch s, ok := r.ReadString('\n'); true{
		case ok != nil:
			fmt.Printf("%d %d %d\n", chars, words, lines)
			return
		default:
			chars += len(s)
			words += len(strings.Fields(s))
			lines ++
		}
	}
}

//仿UXIX的uniq的go实现：  消除重复字符
func Uniq(){
	list := []string{"a", "b", "a", "a", "c", "d", "e"}
	first := list[0]
	fmt.Printf("%s", first)
	for _, v := range list[1:]{
		if first != v{
			fmt.Printf("%s", v)
			first = v
		}
	}
}

//打印自己的程序：quine
func GoQuine() {
	fmt.Printf("%s%c%s%c\n", q, 0x60, q, 0x60)
}
var q = `/* Go quine */
package main
import "fmt"
func main() {
fmt.Printf("%s%c%s%c\n", q, 0x60, q, 0x60)
}`

//实现一个简单的echo服务器
func Echo(c net.Conn){
	defer c.Close()
	line, err := bufio.NewReader(c).ReadString('\n')
	if err != nil{
		fmt.Printf("Failure to read: %s\n",err.Error())
		return
	}
	_, err1 := c.Write([]byte(line))
	if err1 != nil{
		fmt.Printf("failure to write %s\n", err.Error())
		return
	}
}

func Listen_Accept(){
	l, err := net.Listen("tcp", "127.0.0.1:8053")
	if err != nil{
		fmt.Printf("failure to listen: %s\n",err.Error())
	}
	for {
		if c, err := l.Accept(); err == nil{
			go Echo(c)
		}
	}
}
func main() {
	//ProcessGo()
	//GetTheWords()
	//Uniq()
	// GoQuine()
	//Listen_Accept()
}
