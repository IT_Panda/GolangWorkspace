package main

import (
	"fmt"
	"strconv"
)

//这一章讲函数
//递归
func rec(i int){
	if i == 10{
		return
	}
	rec(i + 1)
	fmt.Printf("%d", i)
}

//比较大小
func order(a, b int)(int , int){
	if a > b{
		return b, a
    }
    return a, b
}

type stack struct{
	i int
	data [10]int
}
func (s *stack) push(k int){
	s.data[s.i] = k
	s.i++
}
func (s *stack)pop() int{
	s.i--
	return s.data[s.i]
}
//打印栈内元素的代码
func (s stack)String() string{
	var str string
	for i := 0; i <= s.i; i++{
		str = str + "[" +
			strconv.Itoa(i) + ":" + strconv.Itoa(s.data[i]) + "]"
	}
	return str
}

//变参函数
func prtthem(numbers ...int){
	for _, d := range numbers{
		fmt.Printf("%d\n",d)
	}
}

//菲波那切数列
func fibonacci(value int) []int{
	x := make([]int, value)
	x[0], x[1] = 1, 1
	for n := 2; n < value; n++{
		x[n] = x[n-1] + x[n-2]
	}
	return x
}

//匿名函数外加map的使用相结合
func Map(f func(int) int, l []int) []int{
	j := make([]int, len(l))
	for k, v := range l{
		j[k] = f(v)
	}
	return j
}

//返回一个数组中的最大值和最小值
func MAx_min(arr []int) (max , min int){
	max = arr[0]
	min = arr[0]
	for _, v := range arr{
		if v > max{
			max = v
		}else if v < min{
			min = v
		}
		}
		return
}

//冒泡排序
func bubblesort(n []int){
	for i := 0; i < len(n)-1; i++{
		for j := i+1; j < len(n); j++{
			if n[i] > n[j]{
				n[i], n[j] = n[j], n[i]
			}
		}
	}
}
//返回一个函数
func plusTwo()func(int)int{
	return func(x int)int{
		return x + 2
	}
}

//闭包
func Function(x int) func(int) int{
	return func(y int) int{
		return x + y
	}
}
func main(){
	//函数：
	rec(1)
//匿名函数:函数作为值
	a := func(){
		println("hello")
	}
	a()
	fmt.Printf("\n%T\n", a)

	var s *stack =  new(stack)
	s.push(25)
	s.push(14)
	fmt.Println(s)
	s.String()
	prtthem(1, 4, 5, 7, 4)
	prtthem(1, 2, 4)
	for  _, term := range fibonacci(10){
		fmt.Printf("%v   ", term)
	}
	//匿名函数，函数 既可以做返回值又可以做参数类型
	m := []int{1, 3, 4}
	f := func(i int) int{
		return i * i
	}
	fmt.Println("\n",Map(f, m))

	//最大值和最小值的返回：
	fmt.Println(MAx_min([]int{1, 2, 3, 43, 0, 1, 2}))

	//冒泡排序---  值传递，但是当临时变量使用的时候也会返回回来
	bubble_arr := []int{1, 4, 0, 5 ,6}
	bubblesort(bubble_arr)
	fmt.Println(bubble_arr)
	//返回一个函数，其实也可以认为是咱C++中的函数指针
	p2 := plusTwo()
	fmt.Println(p2(10))
	//闭包的测试用例
	C := Function(10)
	fmt.Println(C(20))

}
