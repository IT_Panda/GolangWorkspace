package main
//----------------------------------------------------指针pointe
import (
	"fmt"
	"sync"
	"bytes"
	"container/list"
	"errors"
	"flag"
	"bufio"
	"io"
	"os"
)
/*
go语言中的指针没有加加操作
go 有两个内存分配原语 new和make
*/
var NILPointe *int
type SyncedBuffer struct {
	lock sync.Mutex
	buffer bytes.Buffer
}
//内存分配
func Memory_Pointe(){
	var p *[]int = new ([]int)
	var v []int = make([]int, 100)

	var p1 *[]int = new([]int)
	*p1 = make([]int, 100, 100)
	v1 := make([]int, 100)

	fmt.Println(p, v, v1)
}


//双向链表的实现
type Value int

type Node struct {
	Value
	prev, next *Node
}

type List struct {
	head, tail *Node
}

func (l *List) Front() *Node{
	return l.head
}

func (n *Node) Next() *Node{
	return n.next
}

func (l *List) Push(v Value) *List{
	n := &Node{Value : v}
	if l.head == nil{
		l.head = n
	}else{
		l. tail.next = n
		n. prev = l.tail
	}
	l.tail = n
	return l
}

var errEmpty = errors.New("List is empty")

func (l *List) Pop() (v Value, err error) {
	if l.tail == nil{
		err = errEmpty
	}else{
		v = l.tail.Value
		l.tail = l.tail.prev
		if l.tail == nil{
			l.head = nil
		}
	}
	return v, err
}

//模拟UNIX和Linux上cat的实现， 支持n输出每行的行号
//定义新的开关'n'，默认是关闭的，容易帮助写入的文章
var numberFlag = flag.Bool("n",false , "number each line")

func Cat(r *bufio.Reader){
	i := 1
	for{
		buf, e := r.ReadBytes('\n')//每次读一行
		if e == io.EOF{//如果到达文件结尾
			break
		}
		if *numberFlag{//如果设定了行号，打印行号然后是内容本身
			fmt.Fprintf(os.Stdout, "%5d  %s", i, buf)
			i++
		}else{//否则仅仅打印该行内容
			fmt.Fprintf(os.Stdout, "%s", buf)
		}
	}
	return
}



//调用库里面的对象方法
func List_Kindel(){
	l := list.New()
	l.PushBack(1)
	l.PushBack(2)
	l.PushBack(3)

	for e := l.Front(); e != nil; e = e.Next(){
		fmt.Printf("%v\n", e.Value)
	}
}


func main() {
	/*fmt.Println(NILPointe)
	Synce := new(SyncedBuffer)
	var SynceOne SyncedBuffer
	fmt.Println(unsafe.Sizeof(Synce), SynceOne)
	Memory_Pointe()*/
	List_Kindel()

	//双向链表：
	l := new(List)
	l.Push(1)
	l.Push(2)
	l.Push(3)
	l.Push(4)
	for n := l.Front(); n != nil; n = n.next{
		fmt.Printf("%v\n", n.Value)
	}

	fmt.Println()
	for v, err := l.Pop(); err == nil ; v, err = l.Pop(){
		fmt.Printf("%v\n", v)
	}

	//cat的模拟实现的测试用例
	flag.Parse()
	if flag.NArg() == 0{
		Cat(bufio.NewReader(os.Stdin))
	}
	for i := 0; i < flag.NArg(); i++{
		f, e := os.Open(flag.Arg(i))
		if e != nil{
			fmt.Fprintf(os.Stderr, "%s: error" +
				"reading from %s:  %s\n",
					os.Args[0], flag.Arg(i), e.Error())
			continue
		}
		Cat(bufio.NewReader(f))
	}
}
