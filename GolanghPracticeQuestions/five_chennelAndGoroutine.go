package main

import (
	"fmt"
	"net/http"
	"io"
	"os"
)
//第一种方法没有做到同步操作，main函数结束则子程序接着结束，导致业务没有处理完整
func shower(c chan int){
	fmt.Println("The channel is Ready")
	for{
		j := <-c
		fmt.Println(j)
	}
}
func showertwo(c chan int, bch chan bool){
	for{
		select{
		case j := <- c:
			fmt.Println(j)
		case <-bch:
			break
		}
	}
}

//斐波那契数列---很不错的一个管道代码
func dup(in <-chan int)(<-chan int, <-chan int, <-chan int){
	a, b, c := make(chan int, 2), make(chan int, 2), make(chan int , 2)
	go func(){
		for {
			x := <-in
			fmt.Printf("The dup x is:%d\n", x)
			a <- x
			b <- x
			c <- x
			fmt.Println("I am dup func()")
		}

	}()
	fmt.Println("I am dup")
	return a, b, c
}
func fib() <-chan int{
	x := make(chan int, 2)
	a, b, out := dup(x)
	go func(){
		x <- 0
		x <- 1
		<- a
		for{
			x<- <-a + <-b
			fmt.Println("I am fib func()")
		}

	}()
	fmt.Println("I am fib")
	return out
}


//demo请求一个网站的首页，并将首页内容打印到标准输出流中
func Demo_Url(){
	fmt.Println("start")
	resp,err:=http.Get("http://example.com/")
	//fmt.Println("start")
	if err!=nil{
		fmt.Println("request fail")
		return
	}
	defer resp.Body.Close()
	io.Copy(os.Stdout,resp.Body)
}

func main(){
	//第一种没有改善的测试用例
	/*ch := make(chan int)
	go shower(ch)
	for i := 0 ; i < 10; i++ {
		ch <- i
	}*/

	//第二种改善过的方法：
	/*ch := make(chan int)
	quit := make(chan bool)
	go showertwo(ch, quit)
	for i := 0; i<10; i++{
		ch <- i
	}
	quit <- true*/

	x := fib()
	for i := 0; i < 5; i++{
		fmt.Println(<- x)
	}
}

