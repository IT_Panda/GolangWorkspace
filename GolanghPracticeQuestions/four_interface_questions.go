package main

import "fmt"

//the question is interface

func Less(l, r interface{})bool{
	switch r.(type){
	case int:
		if _, ok := l.(int); ok{
			return l.(int) > r.(int)
		}
	case float64:
		if _, ok := l.(float64); ok{
			return l.(float64) > l.(float64)
		}
		}
	return false
}

func main(){
	var a , b, c int = 10, 11, 12
	var d, e, f float64 = 12.3, 12.4,12.5
	if	Less(c, b) && Less(b, a){
		fmt.Println(a, b,c)
	}
	if Less(f, e) && Less(e, d){
		fmt.Println(d,e,f)
	}
}
