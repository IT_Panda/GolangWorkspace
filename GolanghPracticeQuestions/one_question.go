package main

import (
	"fmt"
	"unicode/utf8"
)

// for _loop
func For_loop(count int){
	if count < 1{
		panic("The count is not find")//发出警告
	}
	for i := 0; i < count; i++{
		fmt.Println(i)
	}
}

func For_loop_goto(count int){
	i := 0
	Here:
		if count > i{
			println(i)
			i++
			goto Here
		}
}

func For_loop_Array(arr []int){
	for _, v := range arr{
		println(v)
	}
}


func For_loop_Array_Here(arr []int){
	i := 0
	Here:
		if i < len(arr){
			fmt.Println(arr[i])
			i++
			goto Here
		}
}

func For_loop_bytes(bytes []byte){
	for i, v := range bytes{
		fmt.Printf("%c\n", v)
		if i > 2{
			goto Loop
		}
	}
	Loop:
		fmt.Println("This is goto!")
}
//找出被整除的数字，第一种解法
func Fizz_buzz(n int){

	for i := 0; i < n; i++{
		if  i % 3 == 0 && i % 5 == 0{
			fmt.Println("FizzBuzz:", i)
	}else if i % 3 == 0 {
			fmt.Println("Fizz: ", i)
		}else if i % 5 == 0{
			fmt.Println("Buzz:", i)
		}
}
}
//第二种解法
func Fizz_buzz_two(n int){
	const (
		Fizz = 3
		Buzz = 5
	)
	var p bool
	for i := 1; i < n; i++{
		p = false
		if i % Fizz == 0{
			fmt.Printf("Fizz")
			p = true
			}
			if i % Buzz == 0{
				fmt.Printf("Buzz")
				p = true
			}
			if !p{
				fmt.Printf("%v", i)
			}
			fmt.Println()
	}
}

//字符串类型的题
//打印字符A的金字塔  ---- 第一种解法
func GO_A(){
	s1 := []byte{'A'}
	for i := 1; i < 100; i++{
		fmt.Printf("%s\n",s1)
		s1 = append(s1, 'A')
	}
}
//第二种解法;
func GO_A_two(){
	str := "A"
	for i := 0; i< 100; i++{
		fmt.Printf("%s\n", str)
		str = str + "A"
	}
}

//建立一个程序统计字符串里的字符数量：
func String_Count(str string){
	fmt.Println(str)
	count := 0
	s1 := []byte(str)
	fmt.Println(s1)
	fmt.Println('a', 'A', 'y', 'Y')
	for _, i := range s1{
		if i >= 'a' && i <= 'y' || i >= 'A' && i <= 'Y'{
			count++
			fmt.Println(count)
		}
	}
	fmt.Println(count)
}

func String_count_two(str string){
	fmt.Printf("String: %s\nLength: %d, Tunes: %d\n", str, len([]byte(str)), utf8.RuneCount([]byte(str)))
}
//扩展，替换指定位置4开始的三个字符为“abc”----第一种解法：
func Change_byte(str *string){
	s1 := []byte(*str)
	for i, _ := range s1{
		if i == 4{
			s1[4] = 'a'
			s1 = append(s1[4:], 'b')
			s1 = append(s1[5:], 'c')
		}
	}
	fmt.Println(len(s1))
	for i, v := range s1{
		fmt.Printf("%d    %c\n", i , v)
	}
	fmt.Println()
}
//第二种解法：
func Copy_abc(s string){
	r := []rune (s)
	copy(r[4 : 4+3], []rune("abc"))
	fmt.Printf("Before: %s\n", s)
	fmt.Printf("After: %s\n", string(r))
}

//求float数组的平均值---- 第一种方法
func Float64_Len(slices []float64) float64{
	var sum float64
	for _, v := range slices{
		sum += v
	}
	var length  float64 = float64(len(slices))
	sum /= length
	return sum
}
//第二种方法
func Avg_Switch(xs []float64) float64{
	sum := 0.0
	switch len(xs){
	case 0:
		break
	default:
		for _, v := range xs{
			sum += v
		}
		sum /= float64(len(xs))
	}
	return sum
}

//字符串的翻转 ---- 利用go语言的并行平行赋值
func String_High_low(str string ){
	s1 := []rune(str)
	for i, j := 0, len(s1)-1; i < j; i, j = i+1, j -1{//条件必须是并行的,不支持分开++和分开--
		s1[i], s1[j] = s1[j], s1[i]// 同步赋值很好用，不像c++要定义临时变量
	}
	fmt.Printf("%s\n", string(s1))
}
func main() {
	/*
	For_loop(10)
	//For_loop(0)
	For_loop_goto(10)

	array := [...]int{1, 2, 3, 4, 5}
	For_loop_Array(array[:])
	For_loop_Array_Here(array[:])

	bytes := []byte{'a', 'b', 'c', 'd'}
	For_loop_bytes(bytes)

	Fizz_buzz(100)
	GO_A()
	String_Count("asSASA ddd dsjkdsjs dk")
	var str string  = "asSASA ddd dsjkdsjs dk"
	Change_byte(&str)
	fmt.Println(str)
	float_slice := []float64{12.3, 12.4}
	fmt.Println(Float64_Len(float_slice))
	Fizz_buzz_two(100)
	GO_A_two()
	String_count_two("asSASA ddd dsjkdsjs dk")
	Copy_abc("asSASA ddd dsjkdsjs dk")
	String_High_low("I love Xueleilei")
	fmt.Println(Avg_Switch([]float64{12.3, 12.4}))
*/
}
