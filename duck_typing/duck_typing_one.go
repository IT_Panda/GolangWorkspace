package main

import "fmt"

//duck typing one   鸭子类，抽象事物

type O_Retiever struct{
	name string
}

func (r O_Retiever)Get()string{
	return r.name
}

type Retiever interface{
	Get()string
}

func DwonLoad(r Retiever) string{
	return r.Get()
}



func main(){
	var r Retiever
	r = O_Retiever{"I love XXX"}
	fmt.Println(DwonLoad(r))
}
